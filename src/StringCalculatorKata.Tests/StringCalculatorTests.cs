﻿using System;
using System.ComponentModel;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace StringCalculatorKata.Tests
{
    [TestFixture]
    public class StringCalculatorTests
    {
        private StringCalculator calc;

        [TestFixtureSetUp]
        public void Setup()
        {
            calc = new StringCalculator();
        }

        [TestCase("", 0)]
        [TestCase("  ", 0)]
        [TestCase("0", 0)]
        [TestCase("1", 1)]
        [TestCase("  8  ", 8)]
        [TestCase("4   ", 4)]
        [TestCase("   400", 400)]
        public void AddZeroAndOneNumber(string input,double output)
        {
            calc.Add(input).ShouldBeEquivalentTo(output);
        }

        [TestCase("4,2", 6)]
        [TestCase("4,3,2,1", 10)]
        [TestCase("  0  ,  0 , 00 ", 0)]
        [TestCase("  1  ,  2 , -00 ", 3)]
        [TestCase("1\n2",3)]
        [TestCase("2\n2,3", 7)]
        [TestCase("3,2,0\n4", 9)]
        public void AddNumbersWithDelimeters(string input, double output)
        {
            calc.Add(input).ShouldBeEquivalentTo(output);
        }

        [TestCase("//;\n9;4", 13)]
        [TestCase("//Hello\n9", 9)]
        [TestCase("//Hi\n1Hi2", 3)]
        public void AddCustomDelimeter(string input, double output)
        {
            calc.Add(input).ShouldBeEquivalentTo(output);
        }

        [TestCase("-1",ExpectedException = typeof(ArgumentException),ExpectedMessage = "Negatives not allowed: -1")]
        [TestCase("-1,-3,5,-1", ExpectedException = typeof(ArgumentException), ExpectedMessage = "Negatives not allowed: -1,-3,-1")]
        [TestCase("1,-3,-0,2", ExpectedException = typeof(ArgumentException), ExpectedMessage = "Negatives not allowed: -3")]
        public void AddThrowsExceptionOnNegativeNumberAndPrintsThemAll(string input)
        {
            calc.Add(input);    
        }

        
    }
}
