﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringCalculatorKata
{
    public class StringCalculator
    {
        public StringCalculator()
        {
            Delimeters = new[] {",", "\n"};
        }

        public string[] Delimeters { get; set; }

        public double Add(string input)
        {
            input = input.Trim();
            if (input.Length <= 0)
                return 0;

            string[] delimeters = Delimeters;


            if (input.StartsWith("//"))
            {
                Match match = Regex.Match(input, "//(?<delim>[^\n]*)\n");
                if (match != null)
                {
                    string delimCandidate = match.Groups["delim"].Value;
                    if (!String.IsNullOrEmpty(delimCandidate))
                        delimeters = new[] {match.Groups["delim"].Value};
                }

                input = input.Substring(match.Value.Length);
            }

            var negatives = new List<string>();
            double sum = input.Split(delimeters, StringSplitOptions.None)
                .Sum(n =>
                {
                    double nd = Convert.ToDouble(n);
                    if (nd < 0)
                        negatives.Add(n);
                    return nd;
                });
            if (negatives.Count > 0)
                throw new ArgumentException("Negatives not allowed: " + String.Join(",", negatives));

            return sum;
        }
    }
}